package DAO;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.User;

/**
 * Servlet implementation class user
 */
public class userDAO {

	public User findByLoginInfo(String loginId, String password) {

		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBmanager.getConnection();

			// TODO ここに処理を書いていく
			String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			pStmt.setString(2, password);
			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}

			// 必要なデータのみインスタンスのフィールドに追加
			String loginIdData = rs.getString("login_id");
			String nameData = rs.getString("name");
			return new User(loginIdData, nameData);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	public List<User> findAll() {
		Connection conn = null;
		List<User> userList = new ArrayList<User>();
		try {
			// データベースへ接続
			conn = DBmanager.getConnection();

			String sql = "SELECT * FROM user  WHERE is_admin != 1 ";

			// SELECTを実行し、結果表を取得
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			// 結果表に格納されたレコードの内容を
			// Userインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				boolean isAdmin = rs.getBoolean("is_admin");
				Timestamp createDate = rs.getTimestamp("create_date");
				Timestamp updateDate = rs.getTimestamp("update_date");
				User user = new User(id, loginId, name, birthDate, password, isAdmin, createDate, updateDate);
				userList.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;
	}

//新規登録するためのメソッド作る　　インサート文など　スライド０３　DBとの連携　８ページ
	public void InsertNew(String loginId, String password, String username, String date) {

		// どこからどこまでをtryで囲む？
		Connection conn = null;
		PreparedStatement stmt = null;

		try {

			conn = DBmanager.getConnection();

			String insertsql = "INSERT INTO user(login_id,password,name,birth_date,create_date,update_date) VALUES(?,?,?,?,now(),now())";

			stmt = conn.prepareStatement(insertsql);
			stmt.setString(1, loginId);
			stmt.setString(2, password);
			stmt.setString(3, username);
			stmt.setString(4, date);

			stmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {

			// ステートメントインスタンスがnullでない場合、クローズ処理を実行
			if (stmt != null) {

				try {
					stmt.close();
				} catch (SQLException e) {
					// TODO 自動生成された catch ブロック
					e.printStackTrace();
				}
			}
			// コネクションインスタンスがnullでない場合、クローズ処理を実行
			if (conn != null) {
				try {
					conn.close();
				}

				catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}

	}

	// ユーザー情報詳細の ２の③で必要なメソッドをつくる↓
	public User Detail(int id) {

		Connection conn = null;

		PreparedStatement stmt = null;

		try {

			conn = DBmanager.getConnection();

			String Detailsql = "SELECT * FROM user WHERE id = ?";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(Detailsql);
			pStmt.setInt(1, id);

			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}

			// 必要なデータのみインスタンスのフィールドに追加
			String loginIdData = rs.getString("login_id");
			String nameData = rs.getString("name");
			Date birthDateData = rs.getDate("birth_date");
			String password = rs.getString("password");
			boolean isAdmin = rs.getBoolean("is_admin");
			Timestamp createDateData = rs.getTimestamp("create_date");
			Timestamp upDateData = rs.getTimestamp("update_date");

			return new User(id, loginIdData, nameData, birthDateData, password, isAdmin, createDateData, upDateData);

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				// ステートメントインスタンスがnullでない場合、クローズ処理を実行
				if (stmt != null) {
					stmt.close();
				}
				// コネクションインスタンスがnullでない場合、クローズ処理を実行
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();

			}

		}
		return null;

	}

	public void Update(String password, String username, String birthdate, String id) {

		Connection conn = null;
		PreparedStatement pStmt = null;
		try {
			// データベースへ接続
			conn = DBmanager.getConnection();

			// SELECT文を準備
			String sql = "UPDATE user SET name =?, password =?, birth_date=? ,update_date=now() WHERE id =?";

			pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, username);
			pStmt.setString(2, password);
			pStmt.setString(3, birthdate);
			pStmt.setString(4, id);

			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {

			// ステートメントインスタンスがnullでない場合、クローズ処理を実行
			if (pStmt != null) {

				try {
					pStmt.close();
				} catch (SQLException e) {
					// TODO 自動生成された catch ブロック
					e.printStackTrace();
				}
			}
			// コネクションインスタンスがnullでない場合、クローズ処理を実行
			if (conn != null) {
				try {
					conn.close();
				}

				catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}

	}
	
	public void Update2(String username, String birthdate, String id) {

		Connection conn = null;
		PreparedStatement pStmt = null;
		try {
			// データベースへ接続
			conn = DBmanager.getConnection();

			// SELECT文を準備
			String sql = "UPDATE user SET name =?,birth_date=? ,update_date=now() WHERE id =?";

			pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, username);
			pStmt.setString(2, birthdate);
			pStmt.setString(3, id);

			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {

			// ステートメントインスタンスがnullでない場合、クローズ処理を実行
			if (pStmt != null) {

				try {
					pStmt.close();
				} catch (SQLException e) {
					// TODO 自動生成された catch ブロック
					e.printStackTrace();
				}
			}
			// コネクションインスタンスがnullでない場合、クローズ処理を実行
			if (conn != null) {
				try {
					conn.close();
				}

				catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}

	}


	public void Delete(int id) {

		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			conn = DBmanager.getConnection();

			// ここに処理を書いていく

			String sql = "DELETE FROM user WHERE id = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);

			pStmt.setInt(1, id);

			int rs = pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {

			// ステートメントインスタンスがnullでない場合、クローズ処理を実行
			if (stmt != null) {

				try {
					stmt.close();
				} catch (SQLException e) {
					// TODO 自動生成された catch ブロック
					e.printStackTrace();
				}
			}
			// コネクションインスタンスがnullでない場合、クローズ処理を実行
			if (conn != null) {
				try {
					conn.close();
				}

				catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}
	}

	public List<User>  find(String loginId , String name , String startbirthdate , String endbirthdate ) {

		Connection conn = null;
		List<User> userList = new ArrayList<User>();
		List<String> strList = new ArrayList<String>();
		try {
			// データベースへ接続
			conn = DBmanager.getConnection();
			

			String sql = "SELECT * FROM user WHERE is_admin != 1 ";
			
			StringBuilder stringBuilder = new StringBuilder(sql);

					if(!(loginId.equals (""))){
						stringBuilder.append(" AND login_id=?");
						strList.add(loginId);
					}
					
					if(!(name.equals (""))) {
						stringBuilder.append(" AND name LIKE ?");
						strList.add("%"+name+"%");
					}
					
					if(!(startbirthdate.equals (""))) {
						stringBuilder.append(" AND birth_date >= ?");
						strList.add(startbirthdate);
					}
					if(!(endbirthdate.equals (""))) {
						stringBuilder.append(" AND birth_date <?");
						strList.add(endbirthdate);
					}
		      
		    
			PreparedStatement pStmt = conn.prepareStatement(stringBuilder.toString());
			
			for(int i = 0;i < strList.size();i++) {
			pStmt.setString(i+1, strList.get(i));
			}
			
			ResultSet rs = pStmt.executeQuery();
			
			while (rs.next()) {
				int id = rs.getInt("id");
				String loginid = rs.getString("login_id");
				String name1 = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				boolean isAdmin = rs.getBoolean("is_admin");
				Timestamp createDate = rs.getTimestamp("create_date");
				Timestamp updateDate = rs.getTimestamp("update_date");
				User user = new User(id, loginid, name1, birthDate, password, isAdmin, createDate, updateDate);
				userList.add(user);
			
		
			
			}

				
			
			
			} catch (SQLException e) {
				e.printStackTrace();
				return null;
			} finally {
				// データベース切断
				if (conn != null) {
					try {
						conn.close();
					} catch (SQLException e) {
						e.printStackTrace();
						return null;
					}
			
			
			
			}

			}
		return userList;
			
		
	}

}

