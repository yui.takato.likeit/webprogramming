package servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import DAO.userDAO;
import beans.User;
import utill.PasswordEncorder;

/**
 * Servlet implementation class NewRejServlet
 */
public class NewRejServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public NewRejServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// ユーザー一覧～新規登録 4をやると思う
		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("userInfo");

		if (user == null) {

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userLogin.jsp");
			dispatcher.forward(request, response);

			return;
		}

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userAdd.jsp");
		dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		String loginId = request.getParameter("user-loginid");
		String password = request.getParameter("password");
		String password2 = request.getParameter("password-confirm");
		String username = request.getParameter("name");
		String date = request.getParameter("birthdate");

		PasswordEncorder PE = new PasswordEncorder();

		String encodestr = PE.encordPassword(password);

		if (loginId.equals("") || !(password.equals(password2)) || username.equals("") || password.equals("")|| password2.equals("") || date.equals("")) {
			// リクエストスコープにエラーメッセージをセット

			request.setAttribute("errMsg", "入力された内容は正しくありません");
			// 入力したログインIDを画面に表示するために値をセット
			request.setAttribute("loginid", loginId);
			request.setAttribute("name", username);
			request.setAttribute("birthdate", date);
			// ログインjspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userAdd.jsp");
			dispatcher.forward(request, response);

			return;

		}

		userDAO userDAO = new userDAO();

		userDAO.InsertNew(loginId, encodestr, username, date);

		response.sendRedirect("UserListservlet");
	}

}
