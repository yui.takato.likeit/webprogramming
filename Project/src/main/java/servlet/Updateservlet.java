package servlet;

import java.io.IOException;
import java.sql.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import DAO.userDAO;
import beans.User;
import utill.PasswordEncorder;

/**
 * Servlet implementation class Updateservlet
 */
public class Updateservlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Updateservlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	//↓これなに？
		//	response.getWriter().append("Served at: ").append(request.getContextPath());
	
		HttpSession session = request.getSession();
		User user =(User) session.getAttribute("userInfo");
		
	
		if (user == null) {
			
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userLogin.jsp");
			dispatcher.forward(request, response);
	
			return;
		}
		
		
		
		String id = request.getParameter("id");
		
		int Id = Integer.parseInt(id);
		
		userDAO userDAO = new userDAO();

		 User user1 =  userDAO.Detail(Id);

		request.setAttribute("user", user1);
		// ↓EL式 とりあえずスライドと同じで変数はiにしといた。
	

		
	
		
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
		dispatcher.forward(request, response);
	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		request.setCharacterEncoding("UTF-8");
		
		String id = request.getParameter("user-id");
		String loginid = request.getParameter("user-loginid");
		String password = request.getParameter("password");
		String password2 = request.getParameter("password-confirm");
		String username = request.getParameter("user-name");
		String date = request.getParameter("birth-date");
		
		
  
		
        PasswordEncorder pe = new PasswordEncorder();
		
		String encodeStr=pe.encordPassword(password);
	
		
		if (!( password.equals( password2))  || username.equals("")  || encodeStr.equals("") ||  date.equals("")) {
			// リクエストスコープにエラーメッセージをセット
			
			//ユーザー型の変数に新しく作って　　　String id = request.getParameter("user-id")81行目から８５行目くらいまで　←こーゆーのをまとめる
			//request.setAttribute("user", 新しくできたやつ);
			//String id1 = request.getParameter("user-id");
			//String username1 = request.getParameter("user-name");
			//String date1 = request.getParameter("birth-date");
			User use = new User();
			use.setId(Integer.parseInt(id));
			use.setLoginId(loginid);
			use.setName(username);
			use.setBirthDate(Date.valueOf(date));
			
			
		
			
			request.setAttribute("errMsg", "入力された内容は正しくありません");
			
			request.setAttribute("user", use);
			// 入力したログインIDを画面に表示するために値をセット
			
			// ログインjspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
			dispatcher.forward(request, response);
		
		return;
			
	}
		
		
		if(password.equals( "") && password2.equals( "") ) {
			
			//パスワード抜きのメソッドを作る　DAOに　それを呼び出す。
		userDAO userdao = new userDAO();
		userdao.Update2(username,date,id);
	   //変えたとこ↑
		
		}else {
		userDAO userDAO = new userDAO();
		userDAO.Update(encodeStr,username,date,id);
		}
	response.sendRedirect("UserListservlet");
		

		}

}


