package servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import DAO.userDAO;
import beans.User;

/**
 * Servlet implementation class UserDetailservlet
 */
public class UserDetailservlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserDetailservlet() {
		super();
		// Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		// response.getWriter().append("Served at: ").append(request.getContextPath());

		// user.idであってるのか？userList.jspの158あたり
	
		HttpSession session = request.getSession();
		User user =(User) session.getAttribute("userInfo");
		
	
		if (user == null) {
			
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userLogin.jsp");
			dispatcher.forward(request, response);
	
			return;
		}
		
		String id = request.getParameter("id");
		
		int Id = Integer.parseInt(id);
		
		userDAO userDAO = new userDAO();

		 User user1 =  userDAO.Detail(Id);

		request.setAttribute("user", user1);
		// ↓EL式 とりあえずスライドと同じで変数はiにしといた。
	

		// ↓フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userDetail.jsp");
		dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
	}

}
