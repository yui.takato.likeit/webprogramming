package servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import DAO.userDAO;
import beans.User;

/**
 * Servlet implementation class UserListservlet
 */
public class UserListservlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserListservlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session = request.getSession();
		User user =(User) session.getAttribute("userInfo");
		
	
		if (user == null) {
			
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userLogin.jsp");
			dispatcher.forward(request, response);
	
			return;
		}
		
			

		userDAO userDao = new userDAO();
				List<User> userList = userDao.findAll();

				
				
				// リクエストスコープにユーザ一覧情報をセット　・・・・②
				
				
				request.setAttribute("userList", userList);
				
				
			

				// ユーザ一覧のjspにフォワード　・・・・③
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userList.jsp");
				dispatcher.forward(request, response);


	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	
		request.setCharacterEncoding("UTF-8");
	
		String loginId = request.getParameter("user-loginid");
		String name = request.getParameter("user-name");
		String birthdate = request.getParameter("date-start");
		String birthdate2 = request.getParameter("date-end");
		
		userDAO userDAO = new userDAO();
		// sampleのサーブレットと型が違う
		List<User> userList = userDAO.find(loginId, name,birthdate, birthdate2);

		request.setAttribute("userList", userList);
	
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userList.jsp");
		dispatcher.forward(request, response);
		
	}

}
