CREATE DATABASE usermanagement DEFAULT CHARACTER SET utf8;

use usermanagement;

create table user(
id SERIAL primary key auto_increment,
login_id varchar(255) UNIQUE NOT NULL,
name varchar(255)NOT NULL,
birth_date DATE NOT NULL,
password varchar(255) NOT NULL,
is_admin boolean  NOT NULL default false,
create_date DATETIME NOT NULL,
update_date DATETIME NOT NULL

);

INSERT INTO user(login_id,name,birth_date,password,is_admin,create_date,update_date) VALUES ('admin', '�Ǘ���', 19980103, 'password',true,cast( now() as datetime),cast( now() as datetime));

drop database usermanagement;
